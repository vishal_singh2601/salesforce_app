
%dw 2.0
output application/java 
---
[{
    City__c : "Houston",
    Country__c : "Belgium",
    Email__c : "Ana.Davoren@gmail.com",
    First_Name__c : "Alexandra",
    Last_Name__c : "Smitheram",
    Mob_Number__c : "quia",
    Qualification__c : "",
    Webinar_details__c : "consectetur",
  },
{
    City__c : "San Diego",
    Country__c : "Belgium",
    Email__c : "Adeline.Allabush@gmail.com",
    First_Name__c : "Ali",
    Last_Name__c : "Smitherman",
    Mob_Number__c : "exercitation",
    Qualification__c : "vel",
    Webinar_details__c : "dolorem",
  }]
